package com.ArraysCollections;

import java.util.Random;

import static java.lang.Math.abs;


public class ArrayProvider

{
    public int[] Array1, Array2;

    public void CreateArray() {
        Array1 = new int[10];
        Array2 = new int[10];

        for (int i = 0; i < 10; i++) {
            Array1[i] = randomNumber();
            Array2[i] = randomNumber();
        }
    }

    public void printArray(CustomArray array) {
        printArray(array.array, array.size);
    }

    public void printArray(int[] array, int size) {

        for (int i = 0; i < size; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public int randomNumber() {

        Random rand = new Random();
        int randomNum = abs(rand.nextInt()) % 10;
        return randomNum;
    }

    public CustomArray sameNumber() {
        int k = 0;
        int[] NewArray = new int[10];

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (Array1[i] == Array2[j]) {
                    NewArray[k] = Array1[i];
                    k++;
                }
            }
        }
        return new CustomArray(NewArray, k);
    }

    public CustomArray minusArray(){
        int k = 0;
        int[] NewArray = new int[10];

        for (int i = 0; i < 10; i++)
        {
            boolean unique = true;
            for (int j = 0; j < 10; j++)
            {
                if (Array1[i] == Array2[j])
                {
                    unique = false;
                    break;
                }
            }
            if (unique) {
                NewArray[k] = Array1[i];
                k++;
            }
        }
        return new CustomArray(NewArray, k);
    }

    public CustomArray deleteElement() {
        int k = 0;
        int[] NewArray = new int[10];

        for (int i = 0; i < 9; i++)
            {
                boolean unique = true;
                for (int j = i + 1; j < 10; j++)
                {
                    if (Array1[i] == Array1[j])
                    {
                        unique = false;
                        break;
                    }
                }
                if (unique) {
                    NewArray[k] = Array1[i];
                    k++;
                }
            }
            NewArray[k] = Array1[9];
            k++;
            return new CustomArray(NewArray, k);
    }

    public CustomArray findSameElement(){
        int k = 0;
        int[] NewArray = new int[10];

        for (int i = 0; i < 9; i++)
        {
            if (Array1[i] != Array1[i+1])
            {
                NewArray[k] = Array1[i];
                k++;
            }
        }

        if (Array1[8] != Array1[9])
        {
            NewArray[k] = Array1[9];
            k++;
        }

        return new CustomArray(NewArray, k);
    }
}
