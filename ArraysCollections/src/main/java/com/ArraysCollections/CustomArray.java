package com.ArraysCollections;

public class CustomArray {

    public CustomArray(int[] array, int size)
    {
        this.size = size;
        this.array = array;
    }

    public int size = 0;
    public int[] array;
}
