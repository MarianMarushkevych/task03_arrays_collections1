package com.ArraysCollections;

import java.util.Scanner;

public class App {
    private static ArrayProvider provider;
    public static void main(String[] args) {

        provider = new ArrayProvider();
        provider.CreateArray();
        provider.printArray(provider.Array1, 10);
        provider.printArray(provider.Array2, 10);
        System.out.println();

        System.out.println("Menu:");
        System.out.println("1. Union in array");
        System.out.println("2. Division in array");
        System.out.println("3. Delete elements");
        System.out.println("4. Find same elements");

        int choose = readNumber();

        switch (choose) {
            case 1: PrintSameNumber();break;
            case 2: PrintMinusArray();break;
            case 3: PrintUniq();break;
            case 4: FindSameElement();break;
        }
    }

    private static void PrintSameNumber() {
        CustomArray newArray = provider.sameNumber();
        provider.printArray(newArray);
    }

    private static void PrintMinusArray() {
        CustomArray newArray = provider.minusArray();
        provider.printArray(newArray);
    }

    private static void PrintUniq() {
        CustomArray newArray = provider.deleteElement();
        provider.printArray(newArray);
    }

    private static void FindSameElement() {
        CustomArray newArray = provider.findSameElement();
        provider.printArray(newArray);
    }

    private static Scanner in = new Scanner(System.in);
    public static int readNumber() {
        return in.nextInt();
    }
}
